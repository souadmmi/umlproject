-- Active: 1709545609475@@127.0.0.1@3306@startuml

DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS region;


CREATE TABLE region(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100)
);

CREATE TABLE center(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20),
    id_region INT,
    Foreign Key (id_region) REFERENCES region(id)
);
CREATE TABLE promo(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20),
    startDate DATE,
    endDate DATE,
    studentnumber INT,
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id)
);
CREATE TABLE student(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL
);

CREATE TABLE room(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20),
    capacity INT,
    color VARCHAR(20),
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id)
);


CREATE TABLE booking(
    id INT PRIMARY KEY AUTO_INCREMENT,
    startDate DATE,
    endDate DATE,
    id_room INT,
    Foreign Key (id_room) REFERENCES room(id),
    id_promo INT,
    Foreign Key (id_promo) REFERENCES promo(id)
);
CREATE TABLE promo_student(
    id_promo INT,
    id_student INT,
    PRIMARY KEY (id_promo, id_student),
    Foreign Key (id_promo) REFERENCES promo(id) ON DELETE CASCADE,
    Foreign Key (id_student) REFERENCES student(id) ON DELETE CASCADE
); 



INSERT INTO region(name) VALUES
("Auvergnes-Rhône-Alpes"),
("Alsace"),
("Ile-de-France");

INSERT INTO center (name, id_region) VALUES
("Simplon.co", 1),
("Codecademy", 3),
("HumanBooster", 2);

INSERT INTO promo(name, startDate, endDate, studentnumber, id_center) VALUES
("P20", '2023-01-01', '2025-05-09', 15, 1),
("P21", '2023-03-01', '2025-10-10', 10, 3),
("P22", '2022-06-01', '2025-12-02', 13, 2),
("P23", '2021-05-01', '2022-09-05', 19, 1),
("P24", '2023-01-15', '2023-04-21', 18, 3);



INSERT INTO student(name) VALUES
("Rida"),
("Souad"),
("Kawthar"),
("Yassine"),
("Faiza"),
("Abdelhafid"),
("Brandon" ),
("Miaoudiniwniw"),
("Meriem");

INSERT INTO room(name, capacity, color, id_center) VALUES
("pinguin", 20, "blue", 1),
("koala", 25, "blue", 1),
("lion", 15, "blue", 1),
("tigre", 10, "blue", 1),
("cheval", 30, "blue", 1);

INSERT INTO booking (startDate, endDate, id_room, id_promo) VALUES
('2023-01-01', '2025-05-09', 1, 1),
('2023-03-01', '2025-10-10', 2, 2),
('2022-06-01', '2025-12-02', 3, 3),
('2021-05-01', '2022-09-05', 4, 4),
('2023-01-15', '2024-04-21', 5, 5);

INSERT INTO promo_student(id_promo, id_student) VALUES
(1,1),
(1,2),
(2,3),
(2,4),
(3,5),
(3,6),
(4,7),
(4,8),
(5,9),
(2,1),
(2,5),
(2,6);
